package goprinterrorandexit

import (
	"fmt"
	"os"
)

func This(error error) {
	if error != nil {
		fmt.Println(error)
		os.Exit(1)
	}
}
